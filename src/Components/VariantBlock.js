
import './VariantBlock.css'

function VariantBlock(props) {
    let shade = props.shade
    let number = 0 ;
    function colorCode () {
        if (number === 0 || number === 50) {
            number += 50
        } else {
            number+= 100
        }
        return number
    }
    return (
        <div className='variant-container'>
            {shade.map((variants)=>(
                
                <div>
                <div  className = 'variant-block' style = {{ backgroundColor : variants}}></div>
                <div className='block-info'>
                    <p style={{fontWeight: 'bold'}}>{colorCode()}</p>
                    <p>{variants.toUpperCase()}</p>
                </div>
                </div>
            ))}
        </div>
    )
}

export default VariantBlock;