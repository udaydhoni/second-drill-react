
import  VariantBlock  from './VariantBlock';

import './Color.css'

function ColorBlock (props) {
    let data = Object.keys(props.info);
    return (
        <div>
            {data.map((color)=>(
                <div className='main-container'>
                    <div className='left-section'>
                        <h2>{color.charAt(0).toUpperCase() + color.slice(1)}</h2>
                        <p>color.{color.charAt(0).toUpperCase() + color.slice(1)}</p>
                    </div>
                    <VariantBlock shade={props.info[color]}></VariantBlock>
                    
                </div>
            ))}
        </div>
    )
}

export default ColorBlock;