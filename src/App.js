import data from './data.json';
import ColorBlock from './Components/ColorBlock'

function App() {
  return (
    <div>
      <ColorBlock info={data}></ColorBlock>
    </div>
  );
}

export default App;
